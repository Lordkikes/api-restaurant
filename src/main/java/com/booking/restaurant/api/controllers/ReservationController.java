package com.booking.restaurant.api.controllers;


import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.jsons.CreateReservationRest;
import com.booking.restaurant.api.jsons.ReservationRest;
import com.booking.restaurant.api.jsons.RestaurantRest;
import com.booking.restaurant.api.response.BookingResponse;
import com.booking.restaurant.api.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/booking-restaurant" + "/v1")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "reservation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingResponse<String> createReservation(@RequestBody CreateReservationRest createReservationRest) throws BookingException{
        return new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK",
                reservationService.createReservation(createReservationRest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "reservations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingResponse<List<ReservationRest>> getReservations() throws BookingException {
        return new BookingResponse<> ("Succes", String.valueOf(HttpStatus.OK), "OK", reservationService.getReservations());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "reservation" + "/{" + "reservationId"
            + "}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingResponse<ReservationRest> getReservationById(@PathVariable Long reservationId) throws BookingException {
        return new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK",
                reservationService.getReservationById(reservationId));
    }

}
