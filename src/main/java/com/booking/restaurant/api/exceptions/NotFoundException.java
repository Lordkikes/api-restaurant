package com.booking.restaurant.api.exceptions;

import com.booking.restaurant.api.dtos.ErrorDto;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

public class NotFoundException extends BookingException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String code,  String message) {
        super(code, HttpStatus.NOT_FOUND.value(),message);
    }

    public NotFoundException(String code, String message, ErrorDto data) {
        super(code, HttpStatus.NOT_FOUND.value(),message, Arrays.asList(data));
    }
}
