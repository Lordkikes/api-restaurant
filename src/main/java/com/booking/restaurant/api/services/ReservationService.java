package com.booking.restaurant.api.services;

import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.jsons.CreateReservationRest;
import com.booking.restaurant.api.jsons.ReservationRest;
import com.booking.restaurant.api.jsons.RestaurantRest;

import java.util.List;

public interface ReservationService {

    ReservationRest getReservationById(Long reservationId) throws BookingException;

    String createReservation(CreateReservationRest CreateReservationRest) throws BookingException;

    public List<ReservationRest> getReservations() throws BookingException;


}
