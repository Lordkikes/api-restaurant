package com.booking.restaurant.api.services;

import com.booking.restaurant.api.exceptions.BookingException;

public interface CancelReservationService {

    public String deleteReservation(String locator) throws BookingException;
}
