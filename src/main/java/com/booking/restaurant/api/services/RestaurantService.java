package com.booking.restaurant.api.services;

import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.jsons.RestaurantRest;

import java.util.List;

public interface RestaurantService {

    RestaurantRest getRestaurantById(Long restaurantId) throws BookingException;

    public List<RestaurantRest> getRestaurants() throws BookingException;

}
