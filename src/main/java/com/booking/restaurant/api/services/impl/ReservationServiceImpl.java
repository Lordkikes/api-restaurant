package com.booking.restaurant.api.services.impl;

import com.booking.restaurant.api.entities.Reservation;
import com.booking.restaurant.api.entities.Restaurant;
import com.booking.restaurant.api.entities.Turn;
import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.exceptions.InternalServerErrorException;
import com.booking.restaurant.api.exceptions.NotFoundException;
import com.booking.restaurant.api.jsons.CreateReservationRest;
import com.booking.restaurant.api.jsons.ReservationRest;
import com.booking.restaurant.api.jsons.RestaurantRest;
import com.booking.restaurant.api.repositories.ReservationRepository;
import com.booking.restaurant.api.repositories.RestaurantRepository;
import com.booking.restaurant.api.repositories.TurnRepository;
import com.booking.restaurant.api.services.ReservationService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private TurnRepository turnRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    public static final ModelMapper modelMapper = new ModelMapper();

    public ReservationRest getReservation(Long reservationId) throws BookingException {
        return modelMapper.map(getReservationEntity(reservationId), ReservationRest.class);
    }

    @Override
    public ReservationRest getReservationById(Long reservationId) throws BookingException {
        return modelMapper.map(getReservationEntity(reservationId), ReservationRest.class);
    }

    public String createReservation(final CreateReservationRest createReservationRest) throws BookingException {

        final Restaurant restaurantId = restaurantRepository.findById(createReservationRest.getRestaurantId())
                .orElseThrow(() -> new NotFoundException("RESTAURANT_NOT_FOUND", "RESTAURANT_NOT_FOUND"));

        final Turn turn = turnRepository.findById(createReservationRest.getTurnId())
                .orElseThrow(() -> new NotFoundException("TURN_NOT_FOUND", "TURN_NOT_FOUND"));

        if (reservationRepository.findByTurnAndRestaurantId(turn.getName(), restaurantId.getId()).isPresent()) {
            throw new NotFoundException("RESERVATION_ALREADT_EXIST", "RESERVATION_ALREADT_EXIST");
        }

        String locator = generateLocator(restaurantId, createReservationRest);

        final Reservation reservation = new Reservation();
        reservation.setLocator(locator);
        reservation.setPerson(createReservationRest.getPerson());
        reservation.setDate(createReservationRest.getDate());
        reservation.setRestaurant(restaurantId);
        reservation.setTurn(turn.getName());

        try {
            reservationRepository.save(reservation);
        } catch (final Exception e) {
            LOGGER.error("INTERNAL_SERVER_ERROR", e);
            throw new InternalServerErrorException("INTERNAL_SERVER_ERROR", "INTERNAL_SERVER_ERROR");
        }
        return locator;
    }

    @Override
    public List<ReservationRest> getReservations() throws BookingException {
        final List<Reservation> reservationEntity = reservationRepository.findAll();
        return reservationEntity.stream().map(service -> modelMapper.map(service, ReservationRest.class))
                .collect(Collectors.toList());
    }

    private String generateLocator(final Restaurant restaurantId, final CreateReservationRest createReservationRest)
            throws BookingException {
        return restaurantId.getName() + createReservationRest.getTurnId();
    }

    private Reservation getReservationEntity(Long reservationId) throws BookingException {
        return reservationRepository.findById(reservationId)
                .orElseThrow(() -> new NotFoundException("SNOT-404-1", "RESERVATION_NOT_FOUND"));
    }

}
