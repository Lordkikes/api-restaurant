package com.booking.restaurant.api.services.impl;

import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.exceptions.InternalServerErrorException;
import com.booking.restaurant.api.exceptions.NotFoundException;
import com.booking.restaurant.api.repositories.ReservationRepository;
import com.booking.restaurant.api.services.CancelReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CancelReservationServiceImpl implements CancelReservationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelReservationServiceImpl.class);

    @Autowired
    private ReservationRepository reservationRespository;

    public String deleteReservation(String locator) throws BookingException {

        reservationRespository.findByLocator(locator)
                .orElseThrow(() -> new NotFoundException("LOCATOR_NOT_FOUND", "LOCATOR_NOT_FOUND"));

        try {
            reservationRespository.deleteByLocator(locator);
        } catch (Exception e) {
            LOGGER.error("INTERNAL_SERVER_ERROR", e);
            throw new InternalServerErrorException("INTERNAL_SERVER_ERROR", "INTERNAL_SERVER_ERROR");
        }

        return "LOCATOR_DELETED";
    }
}
