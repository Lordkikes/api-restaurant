package com.booking.restaurant.api.controllers;


import com.booking.restaurant.api.exceptions.BookingException;
import com.booking.restaurant.api.jsons.RestaurantRest;
import com.booking.restaurant.api.jsons.TurnRest;
import com.booking.restaurant.api.response.BookingResponse;
import com.booking.restaurant.api.services.RestaurantService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RestaurantControllerTest {

    //Objetos para el RestaurantRest
    private  static final Long RESTAURANT_ID = 1L;
    private static final String NAME = "Burger";
    private static final String DESCRIPTION = "Tipos de Hamburguesas";
    private static final String ADDRES = "AV Siempre Viva";
    private static final String IMAGES_URL = "localhos:8080/images";


    //Respuestas
    private static final String SUCCES_STATUS = "Succes";
    private static final String SUCCES_CODE = "200 OK";
    private static final String OK = "OK";

    public static final RestaurantRest RESTAURANT_REST = new RestaurantRest();
    public static final List<TurnRest> TURN_LIST = new ArrayList<>();
    public static final List<RestaurantRest> RESTAURANT_REST_LIST = new ArrayList<>();

    @Mock
    RestaurantService restaurantService;

    @InjectMocks
    RestaurantController restaurantController;

    @Before
    public void init() throws BookingException {
        MockitoAnnotations.initMocks(this);
        RESTAURANT_REST.setName(NAME);
        RESTAURANT_REST.setDescription(DESCRIPTION);
        RESTAURANT_REST.setAddress(ADDRES);
        RESTAURANT_REST.setId(RESTAURANT_ID);
        RESTAURANT_REST.setImage(IMAGES_URL);
        RESTAURANT_REST.setTurns(TURN_LIST);
        Mockito.when(restaurantService.getRestaurantById(RESTAURANT_ID))
                .thenReturn(RESTAURANT_REST);
    }

    @Test
    public void getRestaurantByIdTest() throws BookingException{
        final BookingResponse<RestaurantRest> response = restaurantController.getRestaurantById(RESTAURANT_ID);
        assertEquals(response.getStatus(), SUCCES_STATUS);
        assertEquals(response.getCode(), SUCCES_CODE);
        assertEquals(response.getMessage(), OK);
        assertEquals(response.getData(), RESTAURANT_REST);
    }

    @Test
    public void getRestaurantsTest() throws BookingException{
        final BookingResponse<List<RestaurantRest>> response = restaurantController.getRestaurants();

        assertEquals(response.getStatus(), SUCCES_STATUS);
        assertEquals(response.getCode(), SUCCES_CODE);
        assertEquals(response.getMessage(), OK);
        assertEquals(response.getData(), RESTAURANT_REST_LIST);

    }

}